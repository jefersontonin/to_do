# README #

### How do I get started? ###

* cd api && gem install bundler && bundle
* cd api && rake db:create && rake db:migrate && rake db:populate
* cd www && npm install && bower install && grunt assetsPrecompile
* cd server && gem install foreman
* sudo vim /etc/hosts
````
127.0.0.1 api.todo.dev
127.0.0.1 todo.dev
````
* cd server && foreman start


##http://todo.saulosantiago.com.br##