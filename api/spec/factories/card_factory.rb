FactoryGirl.define do
  factory :card do |f|
    list
    f.name Faker::App.name
    f.description Faker::Lorem.sentence(3, false, 4)
    f.status_name { %w(todo doing done).sample }
  end
end