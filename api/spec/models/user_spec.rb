require 'spec_helper'

describe 'User' do
  subject { create(:user) }

  it { expect(subject).to be_valid }
  it { expect(subject.name).not_to be_blank }
  it { expect(subject.email).not_to be_blank }
  it { expect(subject.password).not_to be_blank }

  it { expect(build(:user, name: nil)).not_to be_valid }
  it { expect(build(:user, name: subject.name)).not_to be_valid }
  it { expect(build(:user, email: nil)).not_to be_valid }
  it { expect(build(:user, email: 'foo')).not_to be_valid }
  it { expect(build(:user, email: 'foo@foo')).not_to be_valid }
  it { expect(build(:user, password: nil)).not_to be_valid }
end