require 'spec_helper'

describe API::ListsController, type: :controller do
  let(:record) { create(:list) }
  let(:invalid_attributes) {{ name: nil, user_id: nil, only_me: nil }}
  let(:valid_attributes) { attributes_for(:list).merge!({ user_id: create(:user).id }) }

  let(:count) { 1 }

  before do
    session[:user_id] = create(:user, name: "foo#{ count + 1 }", email: "foo#{ count + 1 }@bar.com").id
  end

  it_should_behave_like 'create', 'list'
  it_should_behave_like 'update', 'list'
  it_should_behave_like 'destroy', 'list'

  describe 'lists' do
    subject do
      create(:list)
      get :index, format: :json
    end

    before { subject }
    it { expect(response.body).to be_present  }
    it { expect(JSON.parse(response.body).length).to eql 1 }
    it { expect(JSON.parse(response.body)).to be_kind_of Array }
  end

  describe 'cards' do
    let(:card) { create(:card) }

    subject do
      card
      get :show, id: card.list.id, format: :json
    end

    before { subject }
    it { expect(response.body).to be_present  }
    it { expect(JSON.parse(response.body).length).to eql 1 }
    it { expect(JSON.parse(response.body)).to be_kind_of Array }
  end
end