class User < ActiveRecord::Base
  include BCrypt
  has_secure_password
  before_save :encrypt_password

  has_many :favorites_lists, class_name: FavoriteList, dependent: :destroy
  has_many :lists, through: :favorites_lists

  validates_presence_of :name, :email
  validates_uniqueness_of :name, :email
  validates_format_of :email, with: /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/

  def self.authenticate(email, password)
    user = find_by_email(email)

    if user && BCrypt::Engine.hash_secret(password, user.password_salt) == user.password_digest
      user
    else
      false
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_digest = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end
