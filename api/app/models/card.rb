class Card < ActiveRecord::Base
  STATUSES = {
    'todo' =>  1,
    'doing' => 2,
    'done' =>  3
  }

  attr_accessor :status_name

  has_many :checklists, dependent: :destroy
  belongs_to :list

  validates_presence_of :name, :description, :list, :status
  validates_uniqueness_of :name, scope: :list_id
  validates_inclusion_of :status, in: STATUSES.values,
    message: "{{status}} must be in #{ STATUSES.values.join ',' }"

  def status_name
    STATUSES.key(status)
  end

  def status_name=(val)
    self.status = STATUSES[val].to_i
  end
end