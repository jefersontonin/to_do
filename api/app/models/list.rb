class List < ActiveRecord::Base
  has_many :cards, dependent: :destroy
  has_many :favorite_lists, dependent: :destroy
  belongs_to :user

  validates_presence_of :name, :user
  validates_uniqueness_of :name, scope: :user_id
  validates :only_me, inclusion: { in: [true, false] }

  scope :initial, -> (user_id) { where('only_me=? OR user_id=?', false, user_id) }
end