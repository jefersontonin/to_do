class ApplicationController < ActionController::API
  include ::ActionController::Serialization

  private
  def parse_params
    JSON.parse(params[:model])
  rescue TypeError
    permit_params
  end

  def current_user
    @current_user = session[:user_id].present? ? User.find(session[:user_id]) : nil
  end

  def create_session(user)
    session[:user_id] = user.id
  end

  def sign_out
    session.clear
  end

  def authenticate_member!
    render nothing: true, status: 403 unless current_user.present?
  end
end