class API::FavoriteListsController < ApplicationController
  before_action :authenticate_member!

  def create
    favorite_list = FavoriteList.new parse_params

    if favorite_list.save
      render json: favorite_list, status: 201
    else
      render json: { errors: favorite_list.errors }
    end
  end

  def destroy
    FavoriteList.find(params[:id]).destroy
    render nothing: true
  end

  private
  def permit_params
    params.require(:model).permit(:user_id, :list_id)
  end
end
