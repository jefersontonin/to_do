class API::ChecklistsController < ApplicationController
  before_action :authenticate_member!

  def create
    checklist = Checklist.new parse_params

    if checklist.save
      render json: checklist, status: 201
    else
      render json: { errors: checklist.errors }
    end
  end

  def update
    checklist = Checklist.find params[:id]

    if checklist.update_attributes parse_params
      render json: checklist, status: 201
    else
      render json: { errors: checklist.errors }
    end
  end

  def destroy
    Checklist.find(params[:id]).destroy
    render nothing: true
  end

  private
  def permit_params
    params.require(:model).permit(:card_id, :resolved, :description)
  end
end
