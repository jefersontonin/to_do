class ListSerializer < ActiveModel::Serializer
  attributes :id, :name, :user_id, :only_me, :user_name, :user_email

  has_many :favorite_lists

  def user_name
    self.object.user.name
  end

  def user_email
    self.object.user.email
  end
end
