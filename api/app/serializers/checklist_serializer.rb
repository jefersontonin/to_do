class ChecklistSerializer < ActiveModel::Serializer
  attributes :id, :card_id, :resolved, :description
end