lock '3.2.0'

set :application, 'todo'
set :repo_url, 'git@bitbucket.org:saulosantiago/to_do.git'
set :branch, 'master'

set :deploy_to, '/home/todo/www/'
set :scm, :git

set :format, :pretty
set :log_level, :debug
set :pty, true
set :keep_releases, 3

set :rvm_ruby_version, 'ruby-2.1.3@todo'
set :bundle_gemfile, -> { release_path.join('api/Gemfile') }

set :nvm_path, '/home/todo/.nvm'
set :nvm_node, 'v0.10.32'
set :nvm_map_bins, %w{ node npm bower }

set :npm_target_path, -> { release_path.join('www') }

set :bower_target_path, -> { release_path.join('www') }

set :grunt_target_path, -> { release_path.join('www') }
set :grunt_tasks, %w{ assetsPrecompile }

namespace :deploy do
  desc 'Restart applications'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute <<-CMD
        sudo stop api-todo
        sudo start api-todo
        sudo stop todo
        sudo start todo
      CMD
    end
  end

  before 'bower:install', 'grunt'
  after :publishing, :restart
end