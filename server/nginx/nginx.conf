daemon off;
master_process off;

events {
  worker_connections  1024;
}

http {
  types_hash_max_size 2048;
  server_names_hash_bucket_size 64;

  sendfile on;
  keepalive_timeout 65;

  client_max_body_size  100m;
  include               config/mime.types;

  gzip                    on;
  gzip_static             on;
  gzip_http_version       1.1;
  gzip_proxied            expired no-cache no-store private auth;
  gzip_disable            "MSIE [1-6]\.";
  gzip_vary               on;
  gzip_comp_level         6;
  gzip_types              text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

  access_log            ../shared/log/nginx.access.log;
  error_log             ../shared/log/nginx.error.log info;

  proxy_set_header      Host $host;
  proxy_set_header      X-Real-IP  $remote_addr;
  proxy_set_header      X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header      X-Forwarded-Port $server_port;
  proxy_set_header      X-Forwarded-Host $host;

  map $http_upgrade $connection_upgrade {
      default upgrade;
      '' close;
  }

  # upstreams
  include config/upstreams.conf;

  # servers
  include config/servers/api.conf;
  include config/servers/www.conf;
}