this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  App.commands.setHandler('when:fetched', function(entities, callback, failCallback) {
    var xhrs;
    xhrs = _.chain([entities]).flatten().pluck('_fetch').value();

    $.when.apply($, xhrs).done(function() {
      callback();
    }).fail(function(e) {
      if (failCallback) {
        failCallback();
      }
      App.vent.trigger('throw:error');
    });
  });

  Entities.Base = {
    url: function(path) {
      return config.api_host + path;
    }
  },

  Backbone.Collection =  Backbone.Collection.extend({
    url: function() {
      return Entities.Base.url(_.result(this, 'path'));
    }
  }),

  Backbone.Model = Backbone.Model.extend({
    url: function() {
      return base = Entities.Base.url(_.result(this, 'path'));
    },

    save: function(key, val, options) {
      this.set({ errors: undefined })

      if (this.blacklist !== null) {
        this.attributes = _.omit(this.attributes, this.blacklist);
      }

      Backbone.Model.__super__.save.call(this, key, val, options)
    },

    fetch: function(options) {
      this.set({ errors: undefined })
      Backbone.Model.__super__.fetch.call(this, options)
    },

    hasErrors: function() {
      return !_.isUndefined(this.get('errors'))
    }
  });
});
