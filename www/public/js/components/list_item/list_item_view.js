this.ToDo.module('Components.ListItem', function(ListItem, App, Backbone, Marionette, $, _) {
  ListItem.Layout = Marionette.LayoutView.extend({
    template: 'list_item/templates/layout',
    regions: {
      favoriteRegion: 'div#favorite_region',
      listItemRegion: 'div#list_item_region',
      contentRegion: 'div#content_region'
    },
    className: 'col-sm-6 col-md-6'
  }),

  ListItem.ContentView = Marionette.ItemView.extend({
    template: 'list_item/templates/content',

    templateHelpers: {
      gravatar: function () {
        return 'http://www.gravatar.com/avatar/' + md5(this.user_email) + '/';
      }
    },

    triggers: {
      'click': 'show:button:clicked'
    }
  }),

  ListItem.AddFavoriteView = Marionette.ItemView.extend({
    template: 'list_item/templates/add_favorite',
    className: 'btn btn-info btn-sm',
    triggers: {
      'click': 'add:button:clicked'
    }
  }),

  ListItem.DeleteFavoriteView = Marionette.ItemView.extend({
    template: 'list_item/templates/delete_favorite',
    className: 'btn btn-danger btn-sm',

    triggers: {
      'click': 'delete:button:clicked'
    }
  }),

  ListItem.FavoriteView = Marionette.CollectionView.extend({
    childView: ListItem.DeleteFavoriteView,
    emptyView: ListItem.AddFavoriteView
  }),

  ListItem.ListView = Marionette.ItemView.extend({
    template: 'list_item/templates/list_item',

    ui: {
      title: '.title',
      h2: 'h2 small',
      form: 'form',
      showButton: 'a.show',
      checkbox: "input[type='checkbox']",
      cancelButton: 'a.cancel',
      deleteButton: 'a.delete'
    },


    triggers: {
      'click @ui.title': 'title:clicked',
      'click @ui.cancelButton': 'cancel:button:clicked',
      'click @ui.showButton': 'show:button:clicked',
      'click @ui.deleteButton': 'delete:button:clicked',
      'submit': 'form:submit'
    },

    modelEvents: {
      'change:showForm' : 'handlerForm',
      'change:name' : 'changeName',
      'change:removeMe': 'removeMe'
    },

    behaviors: {
      Errors: {}
    },

    onRender: function() {
      this.ui.form.hide();
      this.ui.checkbox.attr('checked', this.model.get('only_me'));
    },

    changeName: function() {
      this.ui.h2.text(this.model.get('name'))
    },

    handlerForm: function() {
      if (this.model.get('showForm')) {
        this.showForm()
      } else {
        this.hideForm()
      }
    },

    showForm: function() {
      this.trigger('hide:all:forms');
      this.ui.form.show();
      this.ui.title.hide();
    },

    hideForm: function() {
      this.ui.title.show();
      this.ui.form.hide();
    },

    removeMe: function() {
      if (this.model.get('removeMe')) {
        this.$el.parent().parent().remove()
      }
    }
  })
});
