this.ToDo.module('Components.ListItem', function(ListItem, App, Backbone, Marionette, $, _) {
  ListItem.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      this.layout = this.getLayout();
      this.model = options.model;
      this.user = options.user;
      this.favoriteModel = options.model.get('favorite_lists');
      var _self = this;

      this.listenTo(this.layout, 'show', function(){
        _self.listViewRegion();
        _self.contentRegion();
        _self.favoriteRegion();
      });
    },

    getLayout: function() {
      return new ListItem.Layout();
    },

    listViewRegion: function() {
      var view = this.getListView();
      var _self = this;

      this.listenTo(view, 'title:clicked', function(args){
        if (args.model.canEdit(_self.user.get('id'))) {
          args.model.collection.handlerShowedForm(args.model);
        }
      });

      this.listenTo(view, 'cancel:button:clicked', function(args) {
        args.model.collection.handlerShowedForm();
      });

      this.listenTo(view, 'delete:button:clicked', function(args){
        args.model.set({ removeMe: true });
        args.model.destroy();
        socket.emit('list:delete', args.model.attributes);
      });

      this.listenTo(view, 'show:button:clicked', function(args) {
        App.vent.trigger('list:show:visit', args.model.get('user_id'), args.model.get('id'));
      });

      this.listenTo(view, 'form:submit', function(args){
        var data = Backbone.Syphon.serialize(args.view);
        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              socket.emit('list:update', args.model.attributes);
              model.collection.handlerShowedForm();
            };
          }
        });
      });

      this.layout.listItemRegion.show(view);
    },

    getListView: function() {
      return new ListItem.ListView({ model: this.model })
    },

    contentRegion: function() {
      var view = this.getContentView();

      this.listenTo(view, 'show:button:clicked', function(args) {
        App.vent.trigger('list:show:visit', args.model.get('user_id'), args.model.get('id'));
      });

      this.layout.contentRegion.show(view);
    },

    getContentView: function() {
      return new ListItem.ContentView({ model: this.model })
    },

    favoriteRegion: function() {
      var collection = this.favoriteModel.favorited(this.model.get('id'), this.user.get('id'));
      var view = this.getFavoriteView(collection);
      var _self = this;

      this.listenTo(view, 'childview:add:button:clicked', function(args) {
        model = App.request('new:favorite:list', _self.model.get('id'), _self.user.get('id'));
        model.save(null, {
          success: function(model) {
            collection.add(model);
          }
        });

      });

      this.listenTo(view, 'childview:delete:button:clicked', function(args) {
        socket.emit('list:favorite:remove', args.model.attributes);
        args.model.destroy();
      });

      this.layout.favoriteRegion.show(view);
    },

    getFavoriteView: function(collection) {
      return new ListItem.FavoriteView({ collection: collection })
    }
  });

  App.reqres.setHandler('list:item:wraper', function(model, favoriteModel, user) {
    controller = new ListItem.Controller({ model: model, favoriteModel: favoriteModel, user: user });
    return controller.layout;
  })
})
