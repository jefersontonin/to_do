this.ToDo.module('Components.Tooltip', function(Tooltip, App, Backbone, Marionette, $, _) {
  Tooltip.View = Marionette.ItemView.extend({
    template: 'tooltip/templates/tooltip',
    tagName: 'li',
    className: 'error alert alert-danger alert-dismissible',
    attributes: {
      'data-dismiss': 'alert'
    }
  });

  Tooltip.CollectionView = Marionette.CollectionView.extend({
    childView: Tooltip.View,
    tagName: 'ul',
    className: 'nav nav-pills nav-stacked'
  })
});
