this.ToDo.module('HeaderApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'header/show/templates/layout',
    regions: {
      userRegion: '#user_region'
    }
  }),

  Show.User = Marionette.ItemView.extend({
    template: 'header/show/templates/user',

    templateHelpers: {
      gravatar: function () {
        return 'http://www.gravatar.com/avatar/' + md5(this.email) + '/';
      }
    },

    ui: {
      brand: '.navbar-brand',
      favoritesButton: '.favorites',
      loggoutButton: 'a.loggout'
    },

    triggers: {
      'click @ui.loggoutButton': 'loggout:button:clicked',
      'click @ui.favoritesButton': 'favorites:button:clicked',
      'click @ui.brand': 'brand:clicked'
    }
  });
});
