this.ToDo.module('HomeApp.Show', function(Show, App, Backbone, Marionette, $, _) {
  Show.Layout = Marionette.LayoutView.extend({
    template: 'home/show/templates/layout',
    className: 'full-height bg-black',
    regions: {
      homeRegion: 'section#home_region'
    }
  }),

  Show.View = Marionette.ItemView.extend({
    template: 'home/show/templates/show',

    ui: {
      loginButton: 'a.login',
      subscribeButton: 'a.subscribe'
    },

    triggers: {
      'click @ui.loginButton': 'login:button:clicked',
      'click @ui.subscribeButton': 'subscribe:button:clicked'
    }
  });
});
