this.ToDo.module('DashBoardApp.List.Index', function(Index, App, Backbone, Marionette, $, _) {
  Index.Layout = Marionette.LayoutView.extend({
    template: 'dashboard/list/index/templates/layout',
    regions: {
      buttonRegion: 'section#button_region',
      listRegion: 'section#list_region'
    }
  }),

  Index.ButtonView = Marionette.ItemView.extend({
    template: 'dashboard/list/index/templates/button',
    triggers: {
      'click': 'new:button:clicked'
    }
  }),

  Index.ListEmpty = Marionette.ItemView.extend({
    template: 'dashboard/list/index/templates/list_empty',
    className: 'col-sm-6 col-md-12'
  }),

  Index.ListCollection = Marionette.CollectionView.extend({
    childView: Marionette.ItemView,
    emptyView: Index.ListEmpty,
    className: 'row'
  })
});
