this.ToDo.module('DashBoardApp.Card.New', function(New, App, Backbone, Marionette, $, _) {
  New.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      this.layout = this.getLayout();
      this.listId = options.listId;
      this.userId = options.userId;
      this.model = App.request('new:card:entity', this.listId);

      var _self = this;

      this.listenTo(this.layout, 'show', function() {
        _self.formRegion();
      })

      App.mainRegion.show(this.layout);
    },

    getLayout: function() {
      layout = new New.Layout();

      this.listenTo(layout, 'back:button:clicked', function() {
        App.vent.trigger('list:show:visit', this.userId, this.listId);
      });

      return layout;
    },

    formRegion: function() {
      var view = this.getFormView();
      var _self = this;

      this.listenTo(view, 'form:submit', function(args) {
        var data = Backbone.Syphon.serialize(view);

        args.model.set(data);
        args.model.save(null, {
          success: function(model) {
            if (!model.hasErrors()) {
              socket.emit('card:save', args.model.attributes);
              App.vent.trigger('card:show:visit', _self.userId, _self.listId, model.get('id'));
            };
          }
        });
      })

      this.layout.formRegion.show(view)
    },

    getFormView: function() {
      return new New.Form({
        model: this.model
      });
    }
  });
})