this.ToDo.module('SubscribeApp', function(SubscribeApp, App, Backbone, Marionette, $, _) {
  var API;
  this.startWithParent = false;

  SubscribeApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      'subscribe/': 'show'
    }
  });

  API = {
    show: function() {
      App.execute('user::not:logged?', function() {
        App.vent.trigger('visit', '/subscribe/');
        new SubscribeApp.Show.Controller();
      });
    }
  };

  SubscribeApp.on('start', function() {
    new SubscribeApp.Router({ controller: API });
  });

  App.vent.on('subscribe:visit', function() {
    API.show();
  });
});
