this.ToDo = (function(Backbone, Marionette) {
  var App;

  App = new Marionette.Application();

  App.addRegions({
    mainRegion: '[data-region=main]',
    headerRegion: '[data-region=header]'
  });

  App.addInitializer(function() {
    App.module('HeaderApp').start();
    App.module('HomeApp').start();
    App.module('DashBoardApp').start();
    App.module('SubscribeApp').start();
    App.module('LoginApp').start();
  });

  App.on('start', function() {
    Backbone.history.start({
      pushState: true
    });

    $(document).on('click', 'a[data-internal]', function(ev) {
      ev.preventDefault()
    });
  });

  return App;
})(Backbone, Marionette);

$(document).ready(function() {
  window.socket = io();
  ToDo.start();

  if (config.env === 'development') {
    $.getScript('http://localhost:35729/livereload.js');
  }
});