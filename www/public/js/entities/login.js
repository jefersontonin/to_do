this.ToDo.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  var API;

  Entities.Login = Backbone.Model.extend({
    path: '/login/'
  });

  API = {
    login: function() {
      return new Entities.Login()
    }
  };

  App.reqres.setHandler('login:entity', function() {
    return API.login();
  });
});
