'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35729, files;

  grunt.initConfig({
    clean: {
      build: {
        src: ["dist"]
      }
    },

    copy: {
      font: {
        expand: true,
        cwd: 'public/components/bootstrap/dist/fonts/',
        src: '**',
        dest: 'dist/fonts/',
        flatten: true
      },
    },

    concat_css: {
      all: {
        src: [
          "public/components/bootstrap/dist/css/bootstrap.css",
          "public/components/bootstrap/dist/css/bootstrap-theme.css",
          "public/**/*.css"
        ],
        dest: "dist/css/todo.css"
      },
    },

    concat: {
      options: {
        separator: ";"
      },

      dist: {
        src: [
            "public/components/jquery/dist/jquery.js",
            "public/components/handlebars/handlebars.runtime.js",
            "public/components/underscore/underscore.js",
            "public/components/backbone/backbone.js",
            "public/components/backbone.babysitter/backbone.babysitter.js",
            "public/components/backbone.wreqr/backbone.wreqr.js",
            "public/components/marionette/lib/backbone.marionette.js",
            "public/components/marionette.backbone.syphon/lib/backbone.syphon.js",
            "public/components/jquery-md5/js/md5.js",
            "public/components/bootstrap/dist/js/bootstrap.js",
            "dist/templates/**/*.js",
            "public/js/**/*.js",
        ],

        dest: "dist/js/todo.js"
      }
    },


    jst: {
      options: {
        processName: function(filepath) {
          return filepath.replace(/\.us/, '');
        }
      },

      compile: {
        files: {
          "dist/templates/templates.js": ["public/js/**/*.us"]
        }
      }
    },

    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },

    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },

      css: {
        files: ['public/css/**/*.css'],
        tasks: ['develop', 'delayed-livereload', 'assetsPrecompile']
      },

      js: {
        files: [
          'app.js',
          'app/**/*.js',
          'config/*.js',
          'public/js/**/*.js',
          'public/js/**/*.us'
        ],
        tasks: ['develop', 'delayed-livereload', 'assetsPrecompile']
      },

      views: {
        files: [
          'app/views/*.jade',
          'app/views/**/*.jade'
        ],
        options: { livereload: reloadPort }
      }
    }
  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('assetsPrecompile', ['clean', 'copy:font', 'jst', 'concat', 'concat_css']);
  grunt.registerTask('default', ['assetsPrecompile', 'develop', 'watch']);
};
