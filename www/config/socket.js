module.exports = function(io) {
  io.on('connection', function(socket){
    socket.on('list:save', function(list){
      socket.broadcast.emit('list:saved', list);
    });

    socket.on('list:update', function(list){
      socket.broadcast.emit('list:updated', list);
    });

    socket.on('list:delete', function(list){
      socket.broadcast.emit('list:deleted', list);
    });

    socket.on('card:save', function(card){
      socket.broadcast.emit('card:saved', card);
    });

    socket.on('card:update', function(card){
      socket.broadcast.emit('card:updated', card);
    });

    socket.on('card:delete', function(card) {
      socket.broadcast.emit('card:deleted', card);
    });

    socket.on('checklist:update', function(checklist) {
      socket.broadcast.emit('checklist:updated', checklist);
    });

    socket.on('checklist:delete', function(checklist) {
      socket.broadcast.emit('checklist:deleted', checklist);
    });

    socket.on('list:favorite:remove', function(favorite) {
      socket.emit('list:favorite:removed', favorite);
    })
  });
}